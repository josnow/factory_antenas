package imple;

import inter.Antena;

public class Yagi implements Antena{
	
	public int patron = 1;
	public int swr = 2;
	public int potencia = 3;

	@Override
	public String tipoAntena() {
		return "Antena Yagi, Patron: "+patron+". SWR: "+swr+". Potencia: "+potencia;
	}
	
}
