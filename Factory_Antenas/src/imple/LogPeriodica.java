package imple;

import inter.Antena;

public class LogPeriodica implements Antena {

	public int patron = 2;
	public int swr = 1;
	public int potencia = 8;
	
	@Override
	public String tipoAntena() {
		return "Antena LogPeriodica, Patron: "+patron+". SWR: "+swr+". Potencia: "+potencia;
	}

}
